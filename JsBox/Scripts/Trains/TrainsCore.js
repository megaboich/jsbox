﻿/// <reference path="../jquery-2.0.2.js" />

(function () {

    function CellMap(width, height) {
        this.width = width;
        this.height = height;

        this.cells = [];
        for (var i = 0; i < width * height; ++i) {
            this.cells[i] = {
                terrain: (Math.random() * 5 % 4) == 0 ? 0 : 1,
                id: i
            }
        }
        
        this.getCell = function(ix, iy) {
            if (ix < 0 || ix >= this.width || iy < 0 || iy > this.height) {
                throw "Wrong argument in map.getCell x=" + ix + ", y=" + iy;
            }

            return this.cells[ix + iy * this.width];
        }

        this.serialize = function() {
            return JSON.stringify({
                width: this.width,
                height: this.height,
                cells: this.cells
            });
        }
    }

    function genMap(width, height) {
        var map = new CellMap(width, height);
        return map;
    }

    function loadMap(mapId) {
        
    }
    
    function saveMap(map) {
        
    }

    window.TrainsCore = {
        generateNewMap: genMap,
        loadMap: loadMap,
        saveMap: saveMap
    };

})();