﻿/// <reference path="../jquery-2.0.2.js" />

(function () {

    function renderMap(map, canvasId) {
        var c = document.getElementById(canvasId);
        var ctx = c.getContext("2d");

        var viewport = { x: 20, y: 20 };
        var zoom = 1;
        ctx.translate(viewport.x, viewport.y);
        ctx.scale(zoom, zoom);
        

        for (var iy = 0; iy < map.height; iy++) {
            for (var ix = 0; ix < map.width; ix++) {
                var cell = map.getCell(ix, iy);
                var terrain = cell.terrain;

                //draw borders
                ctx.beginPath();
                ctx.moveTo(30, 0);
                ctx.lineTo(0, 20);
                ctx.lineTo(0, 50);
                ctx.lineTo(30, 70);
                ctx.lineTo(60, 50);
                ctx.lineTo(60, 20);
                ctx.lineTo(30, 0);
                ctx.strokeStyle = (Math.random() * 3 % 2 == 0) ? "red" : "green";
                ctx.stroke();

                //draw texture
            
                //move X for next cell
                ctx.translate(60, 0);
            }

            //move Y for next cell
            ctx.translate(map.width * (-60), 50);
        }
        
        ctx.translate(0, map.height * (-50));
        
        ctx.scale(1 / zoom, 1 / zoom);
        ctx.translate(-viewport.x, -viewport.y);
    }

    window.TrainsDraw = {
        renderMap: renderMap
    };

})();