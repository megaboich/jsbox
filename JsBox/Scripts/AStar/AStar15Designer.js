﻿/// <reference path="../jquery-2.0.2.js" />

(function () {

    function gen(container, width, height, maxval) {

        var m = new Matrix(width, height, maxval);
        renderMatrix(container, m);

        console.log("generate field", container, width, height);
        m.log();
    }

    function renderMatrix(container, m) {
        var table = $('<table/>');
        for (var row = 0; row < m.height; ++row) {
            var tr = $('<tr/>');
            for (var col = 0; col < m.width; ++col) {
                var td = $('<td></td>');
                tr.append(td);
                var val = m.getCell(row, col);
                td.text(val);
                if (val === 0) {
                    td.addClass('zero');
                }

                td.click(function () {
                    triggerCell(container, this, 'active');
                });
                td.dblclick(function () {
                    var p = prompt('Enter new value', $(this).text());
                    if (p) {
                        $(this).text(p)
                    }
                });
            }
            table.append(tr);
        }

        $(container).empty().append(table);
    }

    function triggerCell(container, cell, className, opt) {
        opt = opt || {};
        if (!opt.multi) {
            container.find('.' + className).removeClass(className);
        }
        var cellVariable = $(cell);
        cellVariable.toggleClass(className);
    }

    function getData(container) {
        var m = null;
        var tablerows = $(container).find('tr');
        for (var rowI = 0; rowI < tablerows.length; ++rowI) {
            var tr = $(tablerows[rowI]);
            var tablecells = tr.find('td');
            if (m == null) {
                m = new Matrix(tablecells.length, tablerows.length);
            }
            for (var colI = 0; colI < tablecells.length; ++colI) {
                m.setCell(rowI, colI, $(tablecells[colI]).text() | 0);
            }
        }

        return m;
    }

    function shuffleMatrix(container, N) {
        var m = getData(container);
        m.randomize(N || 10);
        renderMatrix(container, m);
    }

    function showresults(container, results) {
        $(container).empty();
        var step = 0;
        for (var i = results.nodes.length - 1; i >=0 ; --i) {
            var n = results.nodes[i];

            var div = $('<div/>').addClass('resultNode');
            $(container).append(div);
            renderMatrix(div, n);
            div.prepend($('<div/>').text(++step + "  =>"));
        }
        $(container).prepend($('<p/>').text("iterations: " + results.iterations));
        $(container).prepend($('<p/>').text("steps: " + results.count));
        $(container).prepend($('<p/>').text("time: " + results.time));
    }

    function edit(container) {
        var m = getData(container);
        var p = prompt('Enter space separated values');
        if (p) {
            var values = p.split(' ');
            var j = 0;
            for (var i = 0; i < values.length; ++i) {
                var val = $.trim(values[i]);
                if (val && val != '') {
                    m.data[j] = val | 0;
                    j++;
                }
            }
        }

        renderMatrix(container, m);
        
    }

    window.AStar15Designer = {
        generate: gen,
        getData: getData,
        render: renderMatrix,
        randomize: shuffleMatrix,
        showresults: showresults,
        edit: edit
    };

})();