﻿(function () {
    function Matrix(arg0, arg1, arg2) {
        if (arg0.width) {   //assume arg0 as Matrix instance to clone
            var m = arg0; //clone
            this.width = m.width;
            this.height = m.height;
            this.cursor = { row: m.cursor.row, col: m.cursor.col }; //location of zero cell;
            this.data = m.data.slice(0); //copy data to new array
            this.F = m.F || 0;
            this.G = m.G || 0;
            this.H = m.H || 0;
            this.parent = m.parent || null;
        } else {
            this.width = arg0;
            this.height = arg1;
            this.cursor = { row: 0, col: 0 };    //location of zero cell;
            this.data = [];

            for (var i = 0, val = 0; i < this.width * this.height; ++i, ++val) {
                if (val > arg2) {
                    val = 1;
                } 
                this.data.push(val);
            }
            this.F = 0;
            this.G = 0;
            this.H = 0;
        }
    }

    Matrix.prototype.getCell = function (row, col) {
        return this.data[row * this.width + col];
    }

    Matrix.prototype.setCell = function (row, col, value) {
        this.data[row * this.width + col] = value;
        if (value === 0) {
            this.cursor = { row: row, col: col };
        }
        this.hash = null;
    }

    Matrix.prototype.MOVE_UP = 1;
    Matrix.prototype.MOVE_DOWN = 2;
    Matrix.prototype.MOVE_LEFT = 3;
    Matrix.prototype.MOVE_RIGHT = 4;

    Matrix.prototype.getRandomMove = function () {
        var randomMove = Math.floor((Math.random() * 4) + 1);
        return randomMove;
    }

    Matrix.prototype.doMove = function (direction) {
        var nR = this.cursor.row;
        var nC = this.cursor.col;
        switch (direction) {
            case this.MOVE_UP:
                nR--;
                break;
            case this.MOVE_DOWN:
                nR++;
                break;
            case this.MOVE_LEFT:
                nC--;
                break;
            case this.MOVE_RIGHT:
                nC++;
                break;
        }

        //check boundaries
        if (nR < 0 || nR >= this.height || nC < 0 || nC >= this.width) {
            return false;
        }
        var c1 = this.getCell(this.cursor.row, this.cursor.col);
        var c2 = this.getCell(nR, nC);
        this.setCell(this.cursor.row, this.cursor.col, c2);
        this.setCell(nR, nC, c1);
        return true;
    }

    Matrix.prototype.randomize = function (count) {
        for (var i = 0; i < count;) {
            var d = this.getRandomMove();
            if (this.doMove(d)) {
                ++i;
            }
        }
    }

    Matrix.prototype.toString = function () {
        var text = "";
        for (var row = 0; row < this.height; ++row) {
            for (var col = 0; col < this.width; ++col) {
                text += this.getCell(row, col)
                text += "\t";
            }
            text += "\n";
        }
        return text;
    }

    Matrix.prototype.getHash = function () {
        if (this.hash) {
            return this.hash;
        } else {
            this.hash = this.data.join('|');
            return this.hash;
        }
    }

    Matrix.prototype.log = function () {
        console.log(this.toString(), this.getHash());
    }

    Matrix.prototype.clone = function () {
        return new Matrix(this);
    }

    window.Matrix = Matrix;
})();


(function () {
    var _openList = {};
    var _closedList = {};
    var _currentNode = null;

    function run(srcM, dstM, maxiterations, gdivider) {
        console.log('AStar15Algorithm run', srcM, dstM);
        var start = new Date().getTime();

        maxiterations = maxiterations || 10000;
        gdivider = gdivider || 10;

        _openList = {};
        _closedList = {};
        _currentNode = null;

        _openList[srcM.getHash()] = srcM.clone(); // 1

        var iteration = 0;
        while (iteration < maxiterations) {
            ++iteration;
            var minFHash = getMinFNodeHash();   // 2-a
            _currentNode = _openList[minFHash];
            _closedList[_currentNode.getHash()] = true;   // 2-b

            if (_currentNode.getHash() == dstM.getHash()) {
                //finish point in the closed list;
                break;
            }

            delete _openList[_currentNode.getHash()]; //remove current from open list

            //Check neighbours
            var c = _currentNode.clone();
            var neighbours = [];
            for (var move = 1; move <= 4; ++move) {
                if (c.doMove(move)) {
                    c.G = _currentNode.G + 1;
                    neighbours.push(c);
                    c = _currentNode.clone();
                }
            }

            for (var ni = 0; ni < neighbours.length; ++ni) {    //2-c
                var nC = neighbours[ni];
                
                if (checkIfInClosedList(nC)) {
                    continue;   //cell in  _closedList
                }
                
                if (!_openList[nC.getHash()]) { // 2-c-2
                    _openList[nC.getHash()] = nC;
                    nC.parent = _currentNode;
                    nC.F = getH(nC, dstM) + Math.floor(nC.G / gdivider);
                }
            }
        }

        // Get the result chain
        var p = _currentNode;
        var result = [];
        while (p != null) {
            result.push(p);
            p = p.parent;
        }

        var end = new Date().getTime();

        return {
            iterations: iteration,
            count: result.length,
            nodes: result,
            time: end - start
        }
    }

    function getH(m1, m2) {
        var h = 0;
        for (var i = 0; i < m1.data.length; ++i) {
            h += (m1.data[i] === m2.data[i]) ? 0 : 1;
        }
        return h;
    }

    function checkIfInClosedList(m) {
        var mh = m.getHash();
        return _closedList[mh] || false;
    }

    function getMinFNodeHash() {
        var minFValue = null;
        var minHash = null;
        for (var nodeHash in _openList) {
            var node = _openList[nodeHash];
            if (node.F < minFValue || minFValue == null) {
                minHash = node.getHash();
                minFValue = node.F;
            }
        }
        return minHash;
    }

    window.AStar15Algorithm = {
        run: run
    };

})();